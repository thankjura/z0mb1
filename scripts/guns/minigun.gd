extends "res://scripts/guns/base.gd"

const OVERHEAD_TIMEOUT = 3

var overheat_time = 0

func _ready():
    BULLET = preload("res://scenes/guns/bullets/minigun_bullet.tscn")
    SHELL = preload("res://scenes/guns/shells/minigun_shell.tscn")
    ENTITY = preload("res://scenes/entities/minigun_entity.tscn")
    SPEED = 1000
    TIMEOUT = 0.1
    OFFSET = Vector2(125, 55)
    CLIMB_OFFSET = Vector2(-50, -40)
    AIM_NAME = "aim_minigun"
    VIEWPORT_SHUTTER = 10
    DROP_VELOCITY = Vector2(400,-400)
    DROP_ANGULAR = 1
    RECOIL = Vector2(-200, 0)
    SPREADING = 0.05
    HEAVINES = 0.6
    EJECT_SHELL_VECTOR = Vector2(0,300)
    ANIM_DEAD_ZONE_TOP = 16

    _reset_view()

func _reset_view():
    get_node("body/barrel_idle").set_visible(true)
    get_node("body/barrel_run").set_visible(false)
    $flash.set_visible(false)

func _fire_start():
    $animation_player.play("fire", -1, 3)
    player.set_mouth(player.MOUTH_AGGRESIVE)

func _eject_shell():
    var v = EJECT_SHELL_VECTOR * rand_range(0.8, 1.2)
    var s = SHELL.instance()
    s.set_global_position($shell_gate.global_position)
    s.set_z_index(-20)
    var global_rot = $shell_gate.global_rotation
    if abs($shell_gate.global_rotation) > PI/2:
        v.x = -v.x
        global_rot = PI - global_rot
    s.set_global_rotation(rand_range(-PI, PI))
    world.add_child(s)
    var impulse = v.rotated(global_rot)
    s.apply_impulse(Vector2(0,0), impulse + player.movement.velocity)

func _muzzle_flash():
    _eject_shell()

func _fire_stop():
    ._fire_stop()
    $animation_player.stop()
    player.set_mouth()
    _reset_view()

func _process(delta):
    if fired:
        if overheat_time < OVERHEAD_TIMEOUT:
            overheat_time += delta
            if overheat_time > OVERHEAD_TIMEOUT:
                overheat_time = OVERHEAD_TIMEOUT
    else:
        if overheat_time > 0:
            overheat_time -= delta
            if overheat_time < 0:
                overheat_time = 0
    var c = overheat_time / OVERHEAD_TIMEOUT
    get_node("body/overheat").set_modulate(Color(c,c,c))