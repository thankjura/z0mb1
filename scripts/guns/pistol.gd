extends "res://scripts/guns/base.gd"

func _ready():
    BULLET = preload("res://scenes/guns/bullets/pistol_bullet.tscn")
    SHELL = preload("res://scenes/guns/shells/pistol_shell.tscn")
    ENTITY = preload("res://scenes/entities/pistol_entity.tscn")
    SPEED = 1200
    TIMEOUT = 0.4
    OFFSET = Vector2(45, -39)
    CLIMB_OFFSET = Vector2(7, 64)
    AIM_NAME = "aim_pistol"
    VIEWPORT_SHUTTER = 0
    DROP_VELOCITY = Vector2(300,-300)
    DROP_ANGULAR = 20
    RECOIL = Vector2(0, 0)
    SPREADING = 0.01
    EJECT_SHELL_VECTOR = Vector2(0, -200)

    ANIM_DEAD_ZONE_BOTTOM = 40

func _muzzle_flash():
    $animation_player.play("fire%d" % (randi()%2+1), -1, 2)