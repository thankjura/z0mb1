extends "res://scripts/main/base_menu.gd"

func _ready():
    INIT_ITEM = "panel/items/cancel"
    selected_item.grab_focus()

func _on_quit_pressed():
    get_tree().quit()

func _on_cancel_pressed():
    global.resume()
    queue_free()
