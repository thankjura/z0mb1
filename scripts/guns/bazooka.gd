extends "res://scripts/guns/base.gd"

const ACCELERATION = 2000
const RELOAD_TIMEOUT = 0.3

var wait_for_reload = 0

func _ready():
    BULLET = preload("res://scenes/guns/bullets/bazooka_rocket.tscn")
    ENTITY = preload("res://scenes/entities/bazooka_entity.tscn")
    TIMEOUT = 2.5
    OFFSET = Vector2(-24, -40)
    CLIMB_OFFSET = Vector2(-24, -40)
    AIM_NAME = "aim_bazooka"
    DROP_VELOCITY = Vector2(300,-300)
    DROP_ANGULAR = 1
    RECOIL = Vector2(0, 0)
    SPEED = 500
    VIEWPORT_SHUTTER = 5
    HEAVINES = 0.4
    ANIM_DEAD_ZONE_BOTTOM = 36

func _reload():
    get_parent().get_owner().gun_reload()

func fire(delta, velocity):
    if wait_ready > 0:
        return
    wait_ready = TIMEOUT

    if not fired:
        fired = true
        _fire_start()

    var f = BULLET.instance()
    var bullet_velocity = _get_bullet_vector()
    var gun_angle = Vector2(1, 0).angle_to(bullet_velocity)
    f.rotate(gun_angle)
    f.set_axis_velocity(bullet_velocity*SPEED + velocity)
    f.local_dump(velocity)
    f.set_applied_force(bullet_velocity*ACCELERATION)
    f.set_global_position(_get_bullet_position(gun_angle))
    var world = get_tree().get_root().get_node("world")
    world.add_child(f)
    wait_for_reload = RELOAD_TIMEOUT

func _process(delta):
    if wait_for_reload > 0:
        wait_for_reload -= delta
        if wait_for_reload <= 0:
            _reload()