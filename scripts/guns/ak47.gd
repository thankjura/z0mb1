extends "res://scripts/guns/base.gd"

const OVERHEAD_TIMEOUT = 3

var overheat_time = 0

func _ready():
    BULLET = preload("res://scenes/guns/bullets/ak47_bullet.tscn")
    SHELL = preload("res://scenes/guns/shells/ak47_shell.tscn")
    ENTITY = preload("res://scenes/entities/ak47_entity.tscn")
    SPEED = 1000
    TIMEOUT = 0.1
    OFFSET = Vector2(87, -48)
    CLIMB_OFFSET = Vector2(10, -18)
    AIM_NAME = "aim_ak47"
    VIEWPORT_SHUTTER = 2
    DROP_VELOCITY = Vector2(400,-400)
    DROP_ANGULAR = 1
    RECOIL = Vector2(-150, 0)
    SPREADING = 0.05
    HEAVINES = 0.3
    EJECT_SHELL_VECTOR = Vector2(-100, -300)

    ANIM_DEAD_ZONE_BOTTOM = 10

func _muzzle_flash():
    $animation_player.play("fire%d" % (randi()%4+1), -1, 2)
    _eject_shell()

func _fire_start():
    player.set_mouth(player.MOUTH_AGGRESIVE)

func _fire_stop():
    ._fire_stop()
    player.set_mouth()

func _get_bullet_velocity(bullet_velocity, player_velocity):
    return bullet_velocity*SPEED+player_velocity

func _process(delta):
    if fired:
        if overheat_time < OVERHEAD_TIMEOUT:
            overheat_time += delta
            if overheat_time > OVERHEAD_TIMEOUT:
                overheat_time = OVERHEAD_TIMEOUT
    else:
        if overheat_time > 0:
            overheat_time -= delta
            if overheat_time < 0:
                overheat_time = 0
    var c = overheat_time / OVERHEAD_TIMEOUT
    get_node("body/overheat").set_modulate(Color(c,c,c))